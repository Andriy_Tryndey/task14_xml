package com.andriy.parser.dom;

import com.andriy.model.Flowers;
import com.andriy.model.GrowingTips;
import com.andriy.model.VisualParameters;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import sun.plugin.dom.core.Document;

import java.util.ArrayList;
import java.util.List;

public class DOMReader {
    public List<Flowers> readDoc(Document doc){
        doc.getDocumentElement().normalize();
        List<Flowers> flowersList = new ArrayList<>();

        NodeList nodeList = doc.getElementsByTagName("flowers");

        for (int i = 0; i <nodeList.getLength(); i++){
            Flowers flowers = new Flowers();
            GrowingTips growingTips;
            VisualParameters visualParameters;

            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;

                flowers.setFlowerId(Integer.parseInt(element.getAttribute("flowerId")));
                flowers.setName(element.getElementsByTagName("name").item(0).getTextContent());
                flowers.setSoil(element.getElementsByTagName("soil").item(0).getTextContent());
                flowers.setOrigin(element.getElementsByTagName("origin").item(0).getTextContent());
                visualParameters = getVisualParameters(element.getElementsByTagName("visualParameters"));
                growingTips = getGrowingTips(element.getElementsByTagName("growingTips"));
                flowers.setMultiplying(element.getElementsByTagName("multiplying").item(0).getTextContent());
                flowers.setVisualParameters(visualParameters);
                flowers.setGrowingTips(growingTips);
                flowersList.add(flowers);
            }
        }
        return flowersList;
    }

    private VisualParameters getVisualParameters(NodeList nodes) {
        VisualParameters visualParameters = new VisualParameters();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE){
            Element element = (Element) nodes.item(0);
            visualParameters.setColorStem(element.getElementsByTagName("colorStem").item(0).getTextContent());
            visualParameters.setColorLeaf(element.getElementsByTagName("colorLeaf").item(0).getTextContent());
            visualParameters.setEverageSizePlant(Integer.parseInt(element.getElementsByTagName("everageSizePlant").item(0).getTextContent()));
        }
        return visualParameters;
    }

    private GrowingTips getGrowingTips(NodeList nodes) {
        GrowingTips growingTips = new GrowingTips();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE){
            Element element = (Element) nodes.item(0);
            growingTips.setTemperature(Integer.parseInt(element.getElementsByTagName("temperature").item(0).getTextContent()));
            growingTips.setLikesLighting(Boolean.parseBoolean(element.getElementsByTagName("likesLighting").item(0).getTextContent()));
            growingTips.setWatering(Double.parseDouble(element.getElementsByTagName("watering").item(0).getTextContent()));
        }
        return growingTips;
    }
}
