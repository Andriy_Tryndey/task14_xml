package com.andriy.model;

public class Flowers {
    private int flowerId;
    private String name;
    private String soil;
    private String origin;
    private VisualParameters visualParameters;
    private GrowingTips growingTips;
    private String multiplying;

    public Flowers() {
    }

    public Flowers(int flowerId, String name, String soil, String origin, VisualParameters visualParameters, GrowingTips growingTips, String multiplying) {
        flowerId = flowerId;
        this.name = name;
        this.soil = soil;
        this.origin = origin;
        this.visualParameters = visualParameters;
        this.growingTips = growingTips;
        this.multiplying = multiplying;
    }

    public int getFlowerId() {
        return flowerId;
    }

    public Flowers setFlowerId(int flowerId) {
        flowerId = flowerId;
        return this;
    }

    public String getName() {
        return name;
    }

    public Flowers setName(String name) {
        this.name = name;
        return this;
    }

    public String getSoil() {
        return soil;
    }

    public Flowers setSoil(String soil) {
        this.soil = soil;
        return this;
    }

    public String getOrigin() {
        return origin;
    }

    public Flowers setOrigin(String origin) {
        this.origin = origin;
        return this;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public Flowers setVisualParameters(VisualParameters visualParameters) {
        this.visualParameters = visualParameters;
        return this;
    }

    public GrowingTips getGrowingTips() {
        return growingTips;
    }

    public Flowers setGrowingTips(GrowingTips growingTips) {
        this.growingTips = growingTips;
        return this;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public Flowers setMultiplying(String multiplying) {
        this.multiplying = multiplying;
        return this;
    }

    @Override
    public String toString() {
        return "Flowers{" +
                "FlowerId=" + flowerId +
                ", name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +
                ", visualParameters=" + visualParameters +
                ", growingTips=" + growingTips +
                ", multiplying='" + multiplying + '\'' +
                '}';
    }
}
