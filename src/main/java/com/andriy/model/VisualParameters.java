package com.andriy.model;

public class VisualParameters {
    private String colorStem;
    private String colorLeaf;
    private int everageSizePlant;

    public VisualParameters() {
    }

    public VisualParameters(String colorStem, String colorLeaf, int everageSizePlant) {
        this.colorStem = colorStem;
        this.colorLeaf = colorLeaf;
        this.everageSizePlant = everageSizePlant;
    }

    public String getColorStem() {
        return colorStem;
    }

    public VisualParameters setColorStem(String colorStem) {
        this.colorStem = colorStem;
        return this;
    }

    public String getColorLeaf() {
        return colorLeaf;
    }

    public VisualParameters setColorLeaf(String colorLeaf) {
        this.colorLeaf = colorLeaf;
        return this;
    }

    public int getEverageSizePlant() {
        return everageSizePlant;
    }

    public VisualParameters setEverageSizePlant(int everageSizePlant) {
        this.everageSizePlant = everageSizePlant;
        return this;
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "colorStem='" + colorStem + '\'' +
                ", colorLeaf='" + colorLeaf + '\'' +
                ", everageSizePlant=" + everageSizePlant +
                '}';
    }
}
