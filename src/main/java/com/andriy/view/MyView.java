package com.andriy.view;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    Scanner sc = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    public MyView(){
        menu = new LinkedHashMap<>();

        menu.put("1", "");
        menu.put("2", "");
        menu.put("3", "");
        menu.put("4", "");
        menu.put("5", "");
        menu.put("Q", "exit");

        methodsMenu = new LinkedHashMap<>();

        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
    }

    private void pressButton1() {

    }

    private void pressButton2() {

    }

    private void pressButton3() {

    }

    private void pressButton4() {

    }

    private void pressButton5() {

    }

    public void show(){
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Select menu point");
            keyMenu = sc.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            }catch (Exception e){}
        }while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        System.out.println("\nMenu");
        for (String str : menu.values()){
            System.out.println(str);
        }
    }
}
