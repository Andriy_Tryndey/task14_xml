<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <body style="font-family: Arial; font-size: 12pt; background-color: #EEE">
                <div style="background-color: #73c373; color: black">
                    <h2>Flowers</h2>
                </div>
                <table border="3">
                    <tr bgcolor="#2E9AFE">
                        <th>flowerId</th>
                        <th>name</th>
                        <th>soil</th>
                        <th>origin</th>
                        <th>colorStem</th>
                        <th>colorLeaf</th>
                        <th>everageSizePlant</th>
                        <th>temperature</th>
                        <th>likesLighting</th>
                        <th>watering</th>
                        <th>multiplying</th>
                    </tr>

                    <xsl:for-each select="flowers/flower">
                        <tr>
                            <td><xsl:value-of select="flowerId"/></td>
                            <td><xsl:value-of select="name"/></td>
                            <td><xsl:value-of select="soil"/></td>
                            <td><xsl:value-of select="origin"/></td>
                            <td><xsl:value-of select="visualParameters/colorStem"/></td>
                            <td><xsl:value-of select="visualParameters/colorLeaf"/></td>
                            <td><xsl:value-of select="visualParameters/everageSizePlant"/></td>
                            <td><xsl:value-of select="growingTips/temperature"/></td>
                            <td><xsl:value-of select="growingTips/likesLighting"/></td>
                            <td><xsl:value-of select="growingTips/watering"/></td>
                            <td><xsl:value-of select="multiplying"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>